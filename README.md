**Teste infomix**

Este repositório possui o codigo fonte do teste proposto pela empresa infomix.

*É recomendado seguir os passo abaixo para implantar e utilizar a aplicação.*

**Requerimentos: **

Para implantar a aplicação é importante possuir uma maquina com as seguintes configurações:
Php 5.4 ou superior
Mysql 5.0 ou superior
e Composer

---

## Passo da implantação


1. Ao baixar os arquivos deste repositorio, entre na raiz do diretorio baixado e execute o seguinte comando:

 **composer install **.

O comando acima ira instalar a dependência do pimple/pimple ~3.0, bem como ira gera o mapeamento de classes, 
segundo as confurações no arquivo composer.json, presente no diretório raiz

2. Crie a estrutura de banco de dados
Entre na pasta **configuracao** na raiz do diretorio baixado e execute o script sql presente no arquivo:  **dump_banco.sql** em 
seu mysql.

3. Definindo dados de conexao com banco de dados na aplicação
Entre na pasta **configuracao** na raiz do diretorio baixado e edite o arquivo:  **banco.json** definindo 
os dados de conexao conforme seu mysql.

4. Configurações extra
Se você estive utilizando apache, nao se esqueça: **ative o modo rewrite para validar os arquivos .htaccess da aplicação**

---

## Executando a aplicação em servidor imbutido do php

Se você possuí uma versão de php 5.4 ou superior, e você tem o php configurado nas variáveis de ambiente de seus sistema operacional.
Você pode executar a servidor imbutido do php para gera um servidor para 
a aplicação executando os seguintes passos:

1. Entre no diretório **diretorRaizAplicacao**
cd diretorRaizAplicacao

2. Execute o comando abaixo, para para gerar um servidor. O parâmetro **NUMEROPORTA** deve ser um numero que não esteja sendo utilizado pelo sistema operacional
<br/>
**php -S localhost:NUMEROPORTA -t public**

3. Execute a url http://localhost:NUMEROPORTA, por exemplo http://localhost:9091 em seu navegado de internet
Agora é só utilizar a interface visual da aplicação.

---