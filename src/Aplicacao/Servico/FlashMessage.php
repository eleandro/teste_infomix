<?php

namespace Aplicacao\Servico;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FlashMessage
 * Classe responsavel por carregar mensagens de alerta ao usuario
 * @author erico.oliveira
 */
 
class FlashMessage {
    //put your code here
    protected static $_templateMessage = 'flash_mensagens.php';
    protected static $_messages    = array();
	private   static $modeloVisao;
    public function __construct( \Aplicacao\View\ModeloVisao $modeloVisao ) {
       $this->startConfig(null); 
	   self::$modeloVisao = $modeloVisao;
    }
    public static function addMessages($typeMessage, $messages)
    {
        self::setTypeMsg($typeMessage);
        if(is_array($messages)){
            foreach($messages as $msg){
                self::loadMessage($typeMessage, $msg);
            }
        } else {
            self::loadMessage($typeMessage, $messages);
        }
        self::persistsMessage();
    }
    
    public static function persistsMessage()
    {
        $namespaceMessages = md5("FlashMessage");
        $_SESSION[$namespaceMessages] = self::$_messages;
    }
    
    public static function getMessagesView($template = null)
    {
		if( is_null( $template ) ) { 
			$template = 
				DIRETORIO_RAIZ_ALICACAO .
				DIRECTORY_SEPARATOR . "telas" . 
				DIRECTORY_SEPARATOR .
				self::$_templateMessage;
		
		}
        $viewVariables = array("_messagesView" => self::getMessagesRecord());
        $html = self::$modeloVisao->loadPartialView($template, $viewVariables);
        return trim($html);
    }
    public static function getMessagesRecord()
    {
       $namespaceMessages = md5("FlashMessage");
       //var_dump(self::$_messages);
       if( !empty($_SESSION[$namespaceMessages]) ){
           $msgs = $_SESSION[$namespaceMessages];
           unset( $_SESSION[$namespaceMessages] );
           return $msgs;
       }
       else{
           return array();
       }  
    }

    public static function loadMessage($typeMessage, $msg)
    {
        self::$_messages[$typeMessage][] = $msg;
    }
    
    public static function setTypeMsg($typeMessage)
    {
       if( empty(self::$_messages[$typeMessage]) ){
            self::$_messages[$typeMessage] = array();
        } 
    }
    
    public function startConfig($templateMessage = null)
    {
        if(!is_null($templateMessage)){
            self::$_templateMessage = $templateMessage;
        }
        if(isset($_SESSION)){
            session_start();
        }else{
			 session_start();
		}
    }
}
