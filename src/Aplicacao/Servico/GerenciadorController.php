<?php

namespace Aplicacao\Servico;

class GerenciadorController {
	
	public function executarProcessosRotaRequiscao( $uri_requisicao, $gerenciadorDependencia )
	{

		$segmentos_uri  = explode( "/", $uri_requisicao );
		$nomeClasseControleRequisicao = $this->obtemNomeClasseControleRequisicao( $segmentos_uri );
		try{
		
		$instanciaControllerExecutar = $gerenciadorDependencia
			->obtemInstanciaDependenciaRecipiente( $nomeClasseControleRequisicao );
		$acaoControllerExecutar      = strtolower(  $segmentos_uri[ 1 ] );
		if ( !method_exists ( $instanciaControllerExecutar , $acaoControllerExecutar ) ) { 
			throw new \RuntimeException( " O caminho digitado na url n�o foi encontrado." );
		}
		$variaveisTela = $instanciaControllerExecutar->{$acaoControllerExecutar}();
		
		return $variaveisTela;
		} catch( \Exception $excecao ) { 
			throw new \RuntimeException( " O caminho digitado na url nao foi encontrado." );
		}
		return array( $nomeClasseControleRequisicao );
			
	}
	
	public function obtemNomeClasseControleRequisicao( $segmentos_uri )
	{
		if( count( $segmentos_uri ) >= 2 ){
				$nomeControleExecutar  = "Aplicacao" . DIRECTORY_SEPARATOR . "Controller" . DIRECTORY_SEPARATOR;
				$nomeClasseControle    = ucfirst( $segmentos_uri[0]); 
				$nomeControleExecutar .= $nomeClasseControle;
				 
				$caminhoDiretorioClassesControle = ".." . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "Aplicacao" . DIRECTORY_SEPARATOR . "Controller" . DIRECTORY_SEPARATOR ;
				$caminhoArquivoClasseControle    = $caminhoDiretorioClassesControle . $nomeClasseControle . ".php";

				if( file_exists( $caminhoArquivoClasseControle ) )
				 {
					 $nomeControleExecutar = 
						str_replace ( DIRECTORY_SEPARATOR, "/", $nomeControleExecutar );
					 return $nomeControleExecutar;
				 }
				  else {
					throw new RuntimeException( " O recurso <b>$segmentos_uri</b> nao foi encontrado.");
				  }
				
			}
	}

}