<?php

namespace Aplicacao\Servico;

class GerenciadorDependencia {
	
	private $caminhoArquivoMapaDependencias = "";
	
	private $diretorioRaizAplicacao;
	
	private $mapaDependenciasAplicacao = array();
	
	private $recepienteDepedencias = null;
	
	public function __construct ( $diretorioRaizAplicacao, \Pimple\Container $recepienteDepedencias )
	{
		
		$this->recepienteDepedencias  = $recepienteDepedencias;
		$this->diretorioRaizAplicacao = $diretorioRaizAplicacao;
		$this->defineDependenciasRepiente( );
		
	}
	
	private function defineDependenciasRepiente(  )
	{
		$this->recepienteDepedencias["Aplicacao/Servico/ConexaoBanco"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\ConexaoBanco( $this->diretorioRaizAplicacao );	
		};
		$this->recepienteDepedencias["Aplicacao/Model/CrudModel"] = function ( $recipiente ) {
			return new \Aplicacao\Model\CrudModel( 
			$recipiente["Aplicacao/Servico/ConexaoBanco"] );	
		};
		
		$this->recepienteDepedencias["Aplicacao/Model/ModelPessoa"] = function ( $recipiente ) {
			return new \Aplicacao\Model\ModelPessoa(  );	
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/GerenciadorLayout"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\GerenciadorLayout( 
				$this->recepienteDepedencias["Aplicacao/View/ModeloVisao"]
			);	
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/GerenciadorController"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\GerenciadorController();	
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/GerenciaAplicacao"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\GerenciaAplicacao( 
				$recipiente["Aplicacao/Servico/GerenciadorController"],
				$recipiente["Aplicacao/Servico/GerenciadorLayout"]
			);
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/FlashMessage"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\FlashMessage(
				$this->recepienteDepedencias["Aplicacao/View/ModeloVisao"]
			);	
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/ClienteRequisicao"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\ClienteRequisicao();	
		};
		
		$this->recepienteDepedencias["Aplicacao/View/ModeloVisao"] = function ( $recipiente ) {
			return new \Aplicacao\View\ModeloVisao(  );	
		};
		
		$this->recepienteDepedencias["Aplicacao/Servico/Validadao/ValidaCampos"] = function ( $recipiente ) {
			return new \Aplicacao\Servico\Validacao\ValidaCampos(  );	
		};
		
		$this->recepienteDepedencias["Aplicacao/View/PopulaFormulario"] = function ( $recipiente ) {
			return new \Aplicacao\View\PopulaFormulario(  );	
		};
		
		$this->recepienteDepedencias["Aplicacao/Controller/Pessoa"] = function ( $recipiente ) {
			return new \Aplicacao\Controller\Pessoa( 
				$recipiente["Aplicacao/Model/CrudModel"],
				$recipiente["Aplicacao/Model/ModelPessoa"],
				$recipiente["Aplicacao/Servico/FlashMessage"],
				$recipiente["Aplicacao/Servico/ClienteRequisicao"],
				$recipiente["Aplicacao/Servico/Validadao/ValidaCampos"],
				$recipiente["Aplicacao/View/PopulaFormulario"]
			);
		};
		
		$this->recepienteDepedencias["Aplicacao/Controller/Index"] = function ( $recipiente ) {
			return new \Aplicacao\Controller\Index(  );
		};
	}
	
	private function obtemMapaDependenciasAplicacao(  )
	{
		return $this->mapaDependenciasAplicacao;
	}
	
	public function obtemInstanciaDependenciaRecipiente( $nomeDependenciaRecipiente )
	{
		
		return $this->recepienteDepedencias[$nomeDependenciaRecipiente];
		
	}
	
}