<?php
/**
 * Description of EncodingString
 * Helper que codifica strings mal formadas do normato definido pela propriedade $_format, que por padrao e definida como UTF-8
 * @author erico.oliveira
 * @since 07/10/2014
 */
class EncodingString {
	
    private static $_format = '';
    
    public static function encoding($data, $format = 'UTF-8')
    {        self::startConfig($format);
            return self::encodingText($data);
    }

    public static function encodingArrayData($data, $dataConvert)
    {
        foreach($data as $position => $value){
            if( self::stringIsMalFormed($value) ){
                $dataConvert[$position] = utf8_encode($value);
            }
            else{
                $dataConvert[$position] = $value;
            }
        }
        return $dataConvert;
    }
    
    public static function encodingStringData($data)
    {
       if( self::stringIsMalFormed($data)  ){
            return utf8_encode($data);
       }
       else{
            return $data;
       } 
    }
    public static function encodingText($data)
    {  
        $dataConvert = array();
        if(is_array($data)){
            return self::encodingArrayData($data, $dataConvert);
        } else{
            return self::encodingStringData($data);
        }
    }

    public static function startConfig($format)
    {
        self::$_format = $format;
    }
    public static function stringIsMalFormed($string)
    {
            /*if ( mb_check_encoding( $string, 'UTF-8' )){
                    return true;
            }
            else{
                    return false;
            }*/
        if (!mb_detect_encoding($string, 'UTF-8', true)) {

            return true;
        } 
        return false;
    }
}
