<?php

namespace Aplicacao\Servico;

class ConexaoBanco {
	
	private $conexao;
	private $caminhoArquivoDadosBanco = "";
	private $dadosParaConexaoArquivoBanco = null;
	
	public function __construct( $diretorioRaizAplicacao )
	{
		$this->obtemDadosParaConexaoArquivoConfiguracaoBanco( $diretorioRaizAplicacao );
		$this->defineConexaoBanco();
	}
	
	public function obtemDadosParaConexaoArquivoConfiguracaoBanco( $diretorioRaizAplicacao )
	{
		$caminhoArquivoBanco  = $diretorioRaizAplicacao;
		$caminhoArquivoBanco  .= DIRECTORY_SEPARATOR;
		$caminhoArquivoBanco  .= "configuracao";
		$caminhoArquivoBanco  .= DIRECTORY_SEPARATOR;
		$caminhoArquivoBanco  .= "banco.json";
		
		$this->caminhoArquivoDadosBanco = $caminhoArquivoBanco;
		if( file_exists($caminhoArquivoBanco) ) { 
			$dadosConexaoArquivoBanco = json_decode( 
			file_get_contents( $caminhoArquivoBanco ), true
			);
			$this->dadosParaConexaoArquivoBanco = $dadosConexaoArquivoBanco;
			
		}
	}
	
	public function defineConexaoBanco()
	{
		if(!is_null( $this->dadosParaConexaoArquivoBanco ) ) { 
			$dadosConexaoBanco = $this->dadosParaConexaoArquivoBanco;
			try{
				$stringConexaoBanco = "mysql:host=" . $dadosConexaoBanco["servidor"] .";";
				$stringConexaoBanco .= "dbname=" .  $dadosConexaoBanco["banco"];
				$conexao = new \Pdo($stringConexaoBanco, $dadosConexaoBanco["usuario"], $dadosConexaoBanco["senha"]);
				$this->conexao = $conexao;
			} catch( \PdoException $exception ) {
				var_dump($exception);
				throw new \RuntimeException("Cheque as configuraçoes no arquivo com dados de acesso ao banco de dados.");
			}
		 }
	}
	
	public function obtemConexaoComBanco(  )
	{
		return $this->conexao;
	}
}