<?php 

namespace Aplicacao\Servico;

class GerenciaAplicacao {
	
	private $gerenciadorController;
	
	private $gerenciadorLayout;
	
	private $diretorioRaizAplicacao;
	
	private $caminhoRequisicao;
	
	public function __construct ( 
	\Aplicacao\Servico\GerenciadorController $gerenciadorController, 
	\Aplicacao\Servico\GerenciadorLayout $gerenciadorLayout
	)
	{
		$this->gerenciadorController = $gerenciadorController;
		$this->gerenciadorLayout       = $gerenciadorLayout;
	}
	
	public function defineDiretorRaizAplicacao( $diretorioRaizAplicacao )
	{
		$this->diretorioRaizAplicacao = $diretorioRaizAplicacao;
	}
	
	public function executaAplicacao( $diretorioRaizAplicacao, $gerenciadorDependencia )
	{
		$this->defineDiretorRaizAplicacao( $diretorioRaizAplicacao );
		
		$this->defineCaminhoRequisicao(  ); 

		$variaveisTelas = $this
			->gerenciadorController
			->executarProcessosRotaRequiscao( 
				$this->caminhoRequisicao, $gerenciadorDependencia );
		if( is_array($variaveisTelas) ) {
			$caminhoArquivosTela = 
				$this->diretorioRaizAplicacao . DIRECTORY_SEPARATOR ."telas";
			
			$this
				->gerenciadorLayout
				->defineCaminhoBaseTelas( $caminhoArquivosTela );
			$this
				->gerenciadorLayout
				->renderizaTela( $this->caminhoRequisicao, $variaveisTelas );
		}elseif( is_string( $variaveisTelas ) ){
			echo $variaveisTelas;
		}
		
		
	}
	
	public function defineCaminhoRequisicao()
	{
		
		$caminho_raiz_requisicao = $_SERVER["PHP_SELF"];
		$caminho_script_index = stristr( $caminho_raiz_requisicao, "/index.php/" );

		if( $caminho_script_index !== false && $caminho_script_index != "/index.php/" ) {
			$uri_requisicao = str_replace( "/index.php/", "", $caminho_script_index );
			$this->caminhoRequisicao = $uri_requisicao;
			return $uri_requisicao;
		} else{

			if( $_SERVER["PHP_SELF"] == "/index.php/" ) {
				$uri_requisicao = "index/index";
				$this->caminhoRequisicao = $uri_requisicao;
				return $uri_requisicao;
			 } else {
				 throw new \RutimeException( "O caminho requisitado não foi encontrado dentro da aplicação" );
			 }
		}
	}
	
	public function obtemCaminhoRequisicao()
	{
		return $this->caminhoRequisicao;
	}
	
}