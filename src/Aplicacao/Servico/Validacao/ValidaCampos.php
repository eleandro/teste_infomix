<?php

namespace Aplicacao\Servico\Validacao;

class ValidaCampos{
	
	private $mensagensValidacao = array();
	
	private $validadores = array();
	
	public function __construct(  ) 
	{
		$this->defineValidadores();
	}
	
	public function validarCampos( $dadosValidar, $validacoes )
	{
		foreach( $validacoes as $campo => $validacao ) {

			$resultadoValidacao = $this->validadores[ $validacao ] ($campo, $dadosValidar);
			if( !empty( $resultadoValidacao ) ) { 
				$this->mensagensValidacao[ $campo ] = $resultadoValidacao;
			 }
		}
		
	}
	
	public function defineValidadores()
	{
		$validadores = array();
		
		$validadores["texto"] = function ( $campo, $dadosValidar )  { 
			if( empty( $dadosValidar[ $campo ] ) ) { 
				return "Preencha o campo '{$campo}' .";
			}
		};
		
		$validadores["email"] = function ( $campo, $dadosValidar )   { 
			if( empty( $dadosValidar[ $campo ] ) ) { 
				return "Preencha o campo '{$campo}' .";
			}else {
				preg_match( "/(.*)(@{1})(.*)/i", $dadosValidar[ $campo ], $padroesCorrespondentes );
				if( empty( $padroesCorrespondentes ) ) { 
					return  "O campo '{$campo}' não está preenchido com dados válidos .";
				}
			}
		};
		
		$validadores["telefone"] = function ( $campo, $dadosValidar ) { 
			if( empty( $dadosValidar[ $campo ] ) ) { 
				return "Preencha o campo '{$campo}' .";
			}else {
				preg_match( "/(\([0-9]{2}\))(\s)([0-9]{1})(\s)([0-9]{4})(\-)([0-9]{4})/i", $dadosValidar[ $campo ], $padroesCorrespondentes );
				if( empty( $padroesCorrespondentes ) ) { 
					return "O campo '{$campo}' não está preenchido com dados válidos .";
				}
			}
		};
		
		$validadores["dataBrasil"] = function ( $campo, $dadosValidar ) { 
			if( empty( $dadosValidar[ $campo ] ) ) { 
				return "Preencha o campo '{$campo}' .";
			}else {
				preg_match( "/([0-9]{2})(\/)([0-9]{2})(\/)([0-9]{4})/i", $dadosValidar[ $campo ], $padroesCorrespondentes );
				if( empty( $padroesCorrespondentes ) ) { 
					return "O campo '{$campo}' não está preenchido com dados válidos .";
				}
			}
		};
		
		$this->validadores = $validadores;
			
	}
	
	public function dadosSaoValidos()
	{
		
		return empty( $this->mensagensValidacao );
		
	}
	
	public function getMensagensValidacao()
	{
		return $this->mensagensValidacao;
	}
}