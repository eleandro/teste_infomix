<?php 

namespace Aplicacao\Servico;

class GerenciadorLayout {
	
	private $modeloVisao;
	
	private $caminhoBaseTelas;
	
	public function __construct( 
		\Aplicacao\View\ModeloVisao $modeloVisao
	)
	{
		$this->modeloVisao = $modeloVisao;
	}
	
	public function defineCaminhoBaseTelas( $caminhoBaseTelas )
	{
		$this->caminhoBaseTelas =  $caminhoBaseTelas;
	}
	
	public function renderizaTela( $nomeArquivo, array $variaveisTela = array() )
	{
			$nomeArquivo = 
				str_replace( "/",  DIRECTORY_SEPARATOR, $nomeArquivo );
			$caminhoArquivoHeader = 
				$this->caminhoBaseTelas . DIRECTORY_SEPARATOR . "header.php";
			$htmlTela  = $this->modeloVisao->loadPartialView( $caminhoArquivoHeader , $variaveisTela );
			$nomeArquivo .= ".php";
			$caminhoArquivoContent = 
				$this->caminhoBaseTelas . DIRECTORY_SEPARATOR . $nomeArquivo;
			$htmlTela .= $this->modeloVisao->loadPartialView( $caminhoArquivoContent, $variaveisTela );
			$caminhoArquivoFooter = 
				$this->caminhoBaseTelas . DIRECTORY_SEPARATOR . "footer.php";
			$htmlTela .= $this->modeloVisao->loadPartialView( $caminhoArquivoFooter, $variaveisTela );
			
			echo $htmlTela;
	}
}