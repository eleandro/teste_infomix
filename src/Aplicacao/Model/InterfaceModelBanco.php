<?php

namespace Aplicacao\Model;

interface InterfaceModelBanco {
	
	public function obtemNomeTabela(  );
	
	public function obtemChavePrimariaTabela(  );
	
	public function defineDadosModel( array $dadosPreencherModel );
	
	public function obtemDadosPersistir(  );
	
}