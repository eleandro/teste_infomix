<?php

namespace Aplicacao\Model;

class CrudModel{
	
	private $conexaoBanco;
	
	public function __construct( \Aplicacao\Servico\ConexaoBanco $conexaoBanco )
	{
		$this->conexaoBanco = $conexaoBanco->obtemConexaoComBanco(  );
	}
	
	public function inserir( \Aplicacao\Model\InterfaceModelBanco $modelBanco )
	{
		$dadosInserir = $modelBanco->obtemDadosPersistir(  );
		$tabelaInserir = $modelBanco->obtemNomeTabela();
		$camposInserir = implode(", ", array_keys( $dadosInserir ));
		$valoresInserir = "'" . implode( "', '",array_values( $dadosInserir) ) ."'";
		$sql = "INSERT INTO {$tabelaInserir} ( $camposInserir ) VALUES ( $valoresInserir ) ";
		
		$declaracaoPdo = $this->conexaoBanco->query( $sql );
		$totalLinhasInseridas = $declaracaoPdo->rowCount(  );
		
		if( $totalLinhasInseridas > 0 ) {
			return $dadosInserir;
		 } else { 
		    return false;
		 }
		
	}
	
	public function atualizar( \Aplicacao\Model\InterfaceModelBanco $modelBanco, $idRegistro )
	{
		$dadosAtualizar     = $modelBanco->obtemDadosPersistir(  );
		$tabelaAtualizar    = $modelBanco->obtemNomeTabela();
		$campoChavePrimaria = $modelBanco->obtemChavePrimariaTabela(  );
		$sql = "UPDATE {$tabelaAtualizar} SET ";
		foreach( $dadosAtualizar as $nomeCampo => $valorCampo){
			$sql .= "{$nomeCampo} = {$valorCampo}, ";
		}
		$sql .= substr( $sql, 0, -2);
		$sql .= " WHERE {$campoChavePrimaria} = '{$idRegistro}'";

		return $this->conexaoBanco->query( $sql );
	}
	
	public function listar( \Aplicacao\Model\InterfaceModelBanco $modelBanco, $id = null)
	{
		$tabelaListarDados = $modelBanco->obtemNomeTabela();
		$campoChavePrimaria = $modelBanco->obtemChavePrimariaTabela(  );
		$sql = "SELECT * FROM {$tabelaListarDados} ";
		if( !is_null( $id) ){
			$sql .= "WHERE {$campoChavePrimaria} = {$id}";
		}
		$declaracaoConsulta = $this
				->conexaoBanco
				->prepare($sql);
		$declaracaoConsulta->execute();
			
		$registros = $declaracaoConsulta->fetchAll( \PDO::FETCH_ASSOC );
		
		return $registros;
		
	}
	
	public function deletar( \Aplicacao\Model\InterfaceModelBanco $modelBanco, $id )
	{
		$tabelaDeletarRegistro = $modelBanco->obtemNomeTabela();
		$campoChavePrimaria = $modelBanco->obtemChavePrimariaTabela(  );
		$sql = "DELETE FROM  {$$tabelaDeletarRegistro} ";
		$sql .= "WHERE {$campoChavePrimaria} = '{$id}'";
		
		return $this->conexaoBanco->query( $sql );
		
	}
}