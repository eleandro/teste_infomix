<?php

namespace Aplicacao\Model;

use Aplicacao\Model\InterfaceModelBanco;

class ModelPessoa implements InterfaceModelBanco {

	private $nomeTabela = "pessoa";
	
	private $chavePrimaria = "id";
	
	private $nome;
	
	private $sobre_nome;
	
	private $email;
	
	private $telefone;
	
	private $nascimento;
	
	public function obtemNomeTabela(  )
	{
		return $this->nomeTabela;
	}
	
	public function obtemChavePrimariaTabela(  )
	{
		return $this->chavePrimaria;
	}
	
	public function defineDadosModel( array $dadosPreencherModel )
	{
		foreach( $dadosPreencherModel as $nomeCampo => $valorCampo ) { 
			$this->{$nomeCampo} = $valorCampo;
		}
	}
	
	public function obtemDadosPersistir(  )
	{
		
		$dataNascimento = \DateTime::createFromFormat("d/m/Y", $this->nascimento);
		return array(
		    'nome'      => $this->nome,
			'sobre_nome' => $this->sobre_nome,
			'email'     => $this->email, 
			'telefone'  => $this->telefone,
			'nascimento' => $dataNascimento->format("Y-m-d") );
	}

}
	
	