<?php

namespace Aplicacao\View;

class PopulaFormulario { 

	protected static $_dadosForm;
    public static  function populate($campo, $tipo,$valorSelected = null, Array $dados = array())
    {
        self::startConfig($dados);
        return self::getMethodPopular($tipo,$campo, $valorSelected);
    }
    
    public static function getMethodPopular($tipo,$campo, $valorSelected = null)
    {
        switch ($tipo ){
            case "text"     : return self::populateText($campo, $valorSelected);
            case "select"   : return self::populateSelect($campo, $valorSelected );
            case "radio"    : return self::populateRadioAndCheckbox($campo, $valorSelected );
            case "checkbox" : return self::populateRadioAndCheckbox($campo, $valorSelected );;
            default         : return self::populateText($campo, $valorSelected);
        }
    }
    
    public static function populateRadioAndCheckbox($campo, $valorSelected = null)
    {
      return ( !empty( self::$_dadosForm[$campo]) && $valorSelected == self::$_dadosForm[$campo] )? " checked='checked' " : "";  
    }

    public static function populateSelect($campo, $valorSelected = null)
    {
       return ( !empty( self::$_dadosForm[$campo]) && $valorSelected == self::$_dadosForm[$campo] )? " selected='selected' " : "";
    }

    public static function populateText($campo, $valorSelected = null)
    {
        return ( !empty( self::$_dadosForm[$campo]) )? self::$_dadosForm[$campo]  : 
            "". (!is_null($valorSelected)? $valorSelected : "" ) ."";
    }

    public static function startConfig($dados)
    {
        if(empty($dados)){
            if(!empty($_POST)) {
               self::$_dadosForm = $_POST;
            }
            if(!empty($_GET)) {
                self::$_dadosForm = $_GET;
            }
        } else{
            self::$_dadosForm = $dados;
        }
    } 

}