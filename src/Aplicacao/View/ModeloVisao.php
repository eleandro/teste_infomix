<?php

namespace Aplicacao\View;

class ModeloVisao { 

	private $caminhoDiretorioRaizAplicacao;
	protected  $_templateHTML;
     protected  $_pathTemplateHtml;
     public function __construct( $pathTemplateHtml = null, Array $viewVariables = array()) {

         $this->startConfig( $pathTemplateHtml , $viewVariables );
        
     }
     public function startConfig($pathTemplateHtml = null, Array $viewVariables = array())
     {
         if( !is_null($pathTemplateHtml) ){
             $this->setPathTemplateHtml($pathTemplateHtml);
             $this->loadPartialView($pathTemplateHtml, $viewVariables );
         }
     }
    public function setPathTemplateHtml($pathTemplateHtml)
    {
        $this->_pathTemplateHtml = $pathTemplateHtml;
    }
    public  function asignVariablesView( Array $viewVariables)
    {
        $result = compact("viewVariables", $viewVariables);
        foreach($result["viewVariables"] as $variable => $value){
            $this->{$variable} = $value;
        }
        
    }
    public  function loadPartialView($pathTemplateHtml = '', Array $viewVariables = array()){
        if(!empty($viewVariables)){
            $this->asignVariablesView($viewVariables);
        }
        return $this->getPartialView($pathTemplateHtml);
    }
    public function getPartialView($pathTemplateHtml = '')
    {
		
        if( file_exists($pathTemplateHtml)){
           ob_start();
           include($pathTemplateHtml) ;
           $contents = trim( ob_get_contents() );
           ob_get_clean();
           $this->setTemplateHTML( $contents );
           //ini_set('display_errors',1);
           return $contents;
        } else {
			var_dump( $pathTemplateHtml ) ; die;
            throw new LogicException("O caminho: '".$pathTemplateHtml."' não é um arquivo válido para template.");
        }
        return NULL;
    }
    public function getTemplateHTML()
    {
        return $this->_templateHTML;
    }
    public function setTemplateHTML($templateHTML)
    {
        $this->_templateHTML = $templateHTML;
    }
	
}