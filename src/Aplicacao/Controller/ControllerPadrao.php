<?php 

namespace Aplicacao\Controller;

class ControllerPadrao {
	
	private $gerenciadorDependencia;
	
	public function __construct (  )
	{
		
	}
	
	public function obtemParametrosPost()
	{
		return $_POST;
	}
	
	public function obtemTipoRequiscao()
	{
		return strtoupper( $_SERVER["REQUEST_METHOD"] );
	}
	
	public function obtemParametrosGet()
	{
		return $_GET;
	}
	
	public function requisicaoIsGet()
	{
		return strtoupper( $_SERVER["REQUEST_METHOD"]) == "GET";
	}
	
	public function requisicaoIsPost()
	{
		return strtoupper( $_SERVER["REQUEST_METHOD"]) == "POST";
	}
	
}