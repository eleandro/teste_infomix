<?php

namespace Aplicacao\Controller;

class Pessoa extends ControllerPadrao {

	private $crudModel;
	
	private $modelPessoa;
	
	private $mensagemNotificacao;
	
	private $clienteRequisicao;
	
	private $validaCampos;
	
	private $populaFormulario;
	
	public function __construct(
		\Aplicacao\Model\CrudModel $crudModel, 
		\Aplicacao\Model\ModelPessoa $modelPessoa, 
		\Aplicacao\Servico\FlashMessage $mensagemNotificacao,
		\Aplicacao\Servico\ClienteRequisicao $clienteRequisicao, 
		\Aplicacao\Servico\Validacao\ValidaCampos $validaCampos,
		\Aplicacao\View\PopulaFormulario $populaFormulario
	)
	{
		$this->crudModel           = $crudModel;
		$this->modelPessoa         = $modelPessoa;
		$this->mensagemNotificacao = $mensagemNotificacao;
		$this->clienteRequisicao   = $clienteRequisicao;
		$this->validaCampos        = $validaCampos;
		$this->populaFormulario    = $populaFormulario;
	}
	
	public function cadastro()
	{
		
		return array();
	}
	
	public function adicionar()
	{
		if( $this->obtemTipoRequiscao(  ) == "POST" ) {
			$parametroRequisicao = $this->obtemParametrosPost();
			//Validacao 
			$validacoes = array( "nome" => "texto", "sobre_nome" => "texto", "nascimento" => "dataBrasil", "email" => "email", "telefone" => "telefone" );
			$this->validaCampos->validarCampos( $parametroRequisicao, $validacoes );
			if( $this->validaCampos->dadosSaoValidos() == true ) { 
				
				$this->modelPessoa->defineDadosModel( $parametroRequisicao );
				
				$dadosInseridos = $this->crudModel->inserir( $this->modelPessoa );
				$retornoClienteRequisicao = $this->clienteRequisicao->enviarDados( $dadosInseridos );
				if( $retornoClienteRequisicao == true ) { 
					$this->mensagemNotificacao->addMessages( "success", array( "Dados Gravados Com sucesso!" ) );
					
				 } else {
					 $this->mensagemNotificacao->addMessages( "error", array( "Ocorreu um erro ao enviar os dados." ) );
				 }
			 
				header( "location: /pessoa/listar" );
			 } else {
				$mesagensValidacao = $this->validaCampos->getMensagensValidacao(  );

				return array( "mesagensValidacao" => $mesagensValidacao,
					"populaFormulario" => $this->populaFormulario
				 );
			 }
		}
	}
	
	public function listar()
	{
		/*$dadosValidar = array( "nome" => "", "telefone" => "(31) 9 9244-4070" );
		$validacoes = array("nome" => "texto", "telefone" => "telefone");
		$this->validaCampos->validarCampos( $dadosValidar, $validacoes );
		var_dump ( $this->validaCampos->getMensagensValidacao() );
		var_dump( $this->validaCampos->dadosSaoValidos() ); die;*/
	
		$mensagemAlerta = $this->mensagemNotificacao->getMessagesView();
		$dadosListar = $this->crudModel->listar( $this->modelPessoa );
		return array( 
			"dadosListar" => $dadosListar,
			"mensagemAlerta" => $mensagemAlerta );
	}
}