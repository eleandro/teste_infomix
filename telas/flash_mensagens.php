<?php
if(!empty($this->_messagesView)):
  $styleMessage = array('alert' => 'alert-warning', 'success' => 'alert-success', 'info' => 'alert-info', 'error' => 'alert-danger');
  foreach( $this->_messagesView as $typeMessage => $messages ):
    if(!empty($this->_messagesView[$typeMessage])):
        foreach( $messages as $msg ):
?>
            <div class="line" style="margin: 0 auto;"> 
                <div style="padding-left: 50px;" class="alert  <?php echo $styleMessage[$typeMessage];?>"> 
                    <?php echo $msg;?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
               </div>
            </div>
<?php
        endforeach;
    endif;
  endforeach;
endif;