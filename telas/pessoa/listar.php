<main role="main" class="container">
	<div class ="row">
		<div class="col-md-12">
			<?php if( !empty( $this->mensagemAlerta ) ) : 
				echo $this->mensagemAlerta;
			 endif; ?>
			
		</div>
		<div class="col-md-12">
			
			<h1>Lista de clientes</h1>
			
			
			<table class="table">
			  <thead>
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">Nome</th>
				  <th scope="col">Sobrenome</th>
				  <th scope="col">Nascimento</th>
				</tr>
			  </thead>
			  <tbody>
				<?php if( !empty ( $this->dadosListar ) ) : ?>
				<?php 	foreach( $this->dadosListar as $pessoa ) : ?>
				<tr>
				  <th scope="row"><?php  echo 	$pessoa["id"]; ?></th>
				  <td><?php  echo 	$pessoa["nome"]; ?></td>
				  <td><?php  echo 	$pessoa["sobre_nome"]; ?></td>
				  <td><?php  echo 	date("d/m/Y", strtotime( $pessoa["nascimento"] ) ); ?></td>
				</tr>
				<?php  endforeach; ?>
				<?php endif; ?>

			  </tbody>
			</table>
			
			<a class= "btn btn-success" href="/pessoa/cadastro" > Adicionar Pessoa</a>
			
			</div>
	</div>
</div>