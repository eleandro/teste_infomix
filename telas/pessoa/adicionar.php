<style>
	.form-text{ color :red !important; }
</style>
<main role="main" class="container">
	<div class="row">
		<div class="col-lg-12">
			<form method="post" action="/pessoa/adicionar" >
			
			  <div class="form-row">
				 <div class="col-md-6 mb-2">
					  <div class="form-group">
						<label for="nome">Nome</label>
						<input type="text" name="nome" value="<?php echo $this->populaFormulario->populate("nome", "text", $_POST["nome"]); ?>" class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Informe o nome">
						<?php if( !empty( $this->mesagensValidacao["nome"] ) ) : ?>
						<small id="emailHelp" class="form-text text-muted"><?php echo $this->mesagensValidacao["nome"]; ?></small>
						<?php endif; ?>
					  </div>
				  </div>
				  <div class="col-md-6 mb-2">
					  <div class="form-group">
						<label for="sobrenome">Sobrenome</label>
						<input type="text" name="sobre_nome" value="<?php echo $this->populaFormulario->populate("sobre_nome", "text", $_POST["sobre_nome"]); ?>" class="form-control" id="sobre_nome" aria-describedby="emailHelp" placeholder="Informe o sobrenome">
						<?php if( !empty( $this->mesagensValidacao["sobre_nome"] ) ) : ?>
						<small id="emailHelp" class="form-text text-muted"><?php echo $this->mesagensValidacao["sobre_nome"]; ?></small>
						<?php endif; ?>
					  </div>
				  </div>
			  </div>
			  
			  <div class="form-row">
				<div class="col-md-4 mb-3">
				  <label for="email">Email</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend1"><i class="far fa-envelope"></i></span>
					</div>
					<input type="text" name="email" value="<?php echo $this->populaFormulario->populate("email", "text", $_POST["email"]); ?>" class="form-control" id="email" placeholder="Preencha com seu email" aria-describedby="inputGroupPrepend1" required>
					<?php if( !empty( $this->mesagensValidacao["email"] ) ) : ?>
						<small id="emailHelp" class="form-text text-muted"><?php echo $this->mesagensValidacao["email"]; ?></small>
					<?php endif; ?>
				  </div>
				</div>
				<div class="col-md-4 mb-3">
				  <label for="telefone">Telefone</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend1"><i class="far fa-phone"></i></span>
					</div>
					<input type="text" name="telefone" value="<?php echo $this->populaFormulario->populate("telefone", "text", $_POST["telefone"]); ?>" class="form-control" id="telefone" placeholder="(00) 0 0000-0000" aria-describedby="inputGroupPrepend1" required>
					<?php if( !empty( $this->mesagensValidacao["telefone"] ) ) : ?>
						<small id="emailHelp" class="form-text text-muted"><?php echo $this->mesagensValidacao["telefone"]; ?></small>
					<?php endif; ?>
				  </div>
				</div>
				<div class="col-md-4 mb-3">
				  <label for="nascimento">Data de Nascimento</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend2"><i class="fal fa-calendar-alt"></i></span>
					</div>
					<input type="text" class="form-control" name="nascimento" value="<?php echo $this->populaFormulario->populate("nascimento", "text", $_POST["nascimento"]); ?>" id="nascimento" placeholder="00/00/0000" aria-describedby="inputGroupPrepend2" required>
					<?php if( !empty( $this->mesagensValidacao["nascimento"] ) ) : ?>
						<small id="emailHelp" class="form-text text-muted"><?php echo $this->mesagensValidacao["nascimento"]; ?></small>
					<?php endif; ?>
				  </div>
				</div>
			  </div>
			  <button type="submit" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
	</div>
</main>