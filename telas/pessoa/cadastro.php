<main role="main" class="container">
	<div class="row">
		<div class="col-lg-12">
			<form method="post" action="/pessoa/adicionar" >
			
			  <div class="form-row">
				 <div class="col-md-6 mb-2">
					  <div class="form-group">
						<label for="nome">Nome</label>
						<input type="text" name="nome" class="form-control" id="nome" aria-describedby="emailHelp" placeholder="Informe o nome">
						<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					  </div>
				  </div>
				  <div class="col-md-6 mb-2">
					  <div class="form-group">
						<label for="sobrenome">Sobrenome</label>
						<input type="text" name="sobre_nome" class="form-control" id="sobre_nome" aria-describedby="emailHelp" placeholder="Informe o sobrenome">
						<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					  </div>
				  </div>
			  </div>
			  
			  <div class="form-row">
				<div class="col-md-4 mb-3">
				  <label for="email">Email</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend1"><i class="far fa-envelope"></i></span>
					</div>
					<input type="text" name="email" class="form-control" id="email" placeholder="Preencha com seu email" aria-describedby="inputGroupPrepend1" required>
				  </div>
				</div>
				<div class="col-md-4 mb-3">
				  <label for="telefone">Telefone</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend1"><i class="far fa-phone"></i></span>
					</div>
					<input type="text" name="telefone" class="form-control" id="telefone" placeholder="(00) 0 0000-0000" aria-describedby="inputGroupPrepend1" required>
				  </div>
				</div>
				<div class="col-md-4 mb-3">
				  <label for="nascimento">Data de Nascimento</label>
				  <div class="input-group">
					<div class="input-group-prepend">
					  <span class="input-group-text" id="inputGroupPrepend2"><i class="fal fa-calendar-alt"></i></span>
					</div>
					<input type="text" class="form-control" name="nascimento" id="nascimento" placeholder="00/00/0000" aria-describedby="inputGroupPrepend2" required>
				  </div>
				</div>
			  </div>
			  <button type="submit" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
	</div>
</main>