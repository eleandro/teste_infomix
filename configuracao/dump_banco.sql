-- MySQL Script generated by MySQL Workbench
-- Tue Jun 12 16:33:08 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema teste_infomix
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema teste_infomix
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `teste_infomix` DEFAULT CHARACTER SET utf8 ;
USE `teste_infomix` ;

-- -----------------------------------------------------
-- Table `teste_infomix`.`pessoa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teste_infomix`.`pessoa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(250) NULL,
  `sobre_nome` VARCHAR(250) NULL,
  `email` VARCHAR(100) NULL,
  `telefone` VARCHAR(20) NULL,
  `nascimento` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
