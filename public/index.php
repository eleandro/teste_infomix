<?php
	
	namespace Aplicacao;
	
	require "../vendor/autoload.php";
	
	$diretorioRaizAplicacao = __DIR__ . DIRECTORY_SEPARATOR . "..";
	
	define(DIRETORIO_RAIZ_ALICACAO, $diretorioRaizAplicacao );
	
	use Aplicacao\Servico\GerenciadorDependencia;
	//use Aplicacao\Servico\GerenciadorController;
	//use Aplicacao\Servico\GerenciadorView;
	//use Aplicacao\Servico\GerenciaAplicacao;
	use Pimple\Container;
	//var_dump( new \PDO('mysql:host=localhost;dbname=test_connection', "root", "senha") );
	//die;
	
	try{
	$recipienteDependencias = new Container();
	$gerencidorDependencias = new GerenciadorDependencia( $diretorioRaizAplicacao, $recipienteDependencias );
	$gerenciaAplicacao = $gerencidorDependencias
		->obtemInstanciaDependenciaRecipiente( "Aplicacao/Servico/GerenciaAplicacao" );

	$gerenciaAplicacao->executaAplicacao( $diretorioRaizAplicacao, $gerencidorDependencias );
	
	die;
	
	//{ "classes" : [ { "classe" : "\Aplicacao\Servico\GerenciaAplicacao" ,  "dependencias" :["tesete", "teste2" ] }  ] }
	}catch ( \Exception $excecao ) {  
		var_dump( $excecao->getMessage() ); die;
	}